ProductModel
============

This entity is used to manage product models.

.. Attention:: Product models is normally only used when NewLogic Sales is setup to show product models with variants.

Properties
^^^^^^^^^^

``ExternalProductModelID``
    Unique identifier in the backend. The unique identifier must never change after the ProductModel has been created. The unique identifier is not visible for the user.
``ProductModelTitle``
    A title that represents the product model.
``ProductModelNo``
    A number or string that represents the product model.
``ShortDescription``
    A short product model description (typical one line).
``LongDescription``
    A long product model description (typical multiple lines).
``ExternalProductCategoryID``
    An unique identifier that represents a ProductCategory that the product model should be linked to.
``ExternalProductCollectionID``
    An unique identifier that represents a ProductCollection that the product model should be linked to.
``Sort``
    Not used. Should be set to 0.
