ProductCollection
=================

This entity is used to manage product collection.

A product collection can be used in NewLogic Sales to filter and sort products and product models. 
A `Product <Product.html>`_ and `ProductModel <ProductModel.html>`_ can be linked to a product collection by the ``ExternalProductCollectionID`` property.

As an example, the fashion industry use product collections to organize products and product models, 
like 'Autumn/Winter 2016' and 'Spring/Summer 2017'.

.. Note:: NewLogic Sales can be setup to show product models with variants. When this setup is used, 
          it is not nessesary to link the `Product <Product.html>`_ to a product collection.
          Only the `ProductModel <ProductModel.html>`_ needs to be linked.

Properties
^^^^^^^^^^

``ExternalProductCollectionID``
    Unique identifier in the backend. The unique identifier must never change after the ProductCollection has been created. 
    The unique identifier is not visible for the user.
``ProductCollectionName``
    A name that represents the product collection.
``ProductCollectionNo``
    A number or string that represents the product collection.
``Sort``
    A number that desides the sorting order within the product collections.
