Company
=======

This entity is used to manage companies. 

A company can be a customer, prospect, supplier or any other type.

Properties
^^^^^^^^^^

``ExternalCompanyID``
    Unique identifier in the backend. The unique identifier must never change after the Company has been created. 
    The unique identifier is not visible for the user.
``CompanyNo``
    String.
``CompanyName``
    String.
``Address1``
    String.
``Address2``
    String.
``City``
    String.
``PostalCode``
    String.
``State``
    String.
``CountryName``
    String.
``PhoneNumber``
    String.
``EmailAddress``
    String.
``Website``
    String.
``CurrencyCode``
    An ISO 4217 alphabetic 3-letter code. This property is used to find a specific product price within a `PriceGroup <PriceGroup.html>`_. 
    If you can't provide this information, you should hardcode a default value like "DKK" or "EUR".
``ExternalPriceGroupID``
    An unique identifier that represents the `PriceGroup <PriceGroup.html>`_ that should be used when creating orders to this company.
    If you can't provide this information, you should hardcode an ID that represents a default price group.
