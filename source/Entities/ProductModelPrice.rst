ProductModelPrice
=================

This entity is used to manage product model prices. 

.. Attention:: Product model prices is normally only used when NewLogic Sales is setup to show product models with variants. 

Properties
^^^^^^^^^^