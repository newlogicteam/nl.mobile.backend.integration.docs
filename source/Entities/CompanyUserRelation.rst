CompanyUserRelation
===================

This entity is used to manage which companies that should be available to which users.

The concept is, that you can create any custom rule that descides which companies that should be syncronized to specific users. 
Some users should see their own customers only, others should see companies in a country or region and some should see all companies. 
The rules for this must be managed in your integration program.

Properties
^^^^^^^^^^

``ExternalCompanyID``
    The unique company identifier that represents the company that should be assigned to a user.
``ExternalUserID``
    The unique user identifier that represents the user that should be assigned to a company.