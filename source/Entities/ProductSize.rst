ProductSize
===========

This entity is used to manage product sizes.

.. Attention:: Product sizes is normally only used when NewLogic Sales is setup to show product models with variants. 
               The variants are combinations of product sizes and product colors. 

A `Product <Product.html>`_ can be linked to a product size by the ``ExternalProductSizeID`` property.

Properties
^^^^^^^^^^

``ExternalProductSizeID``
    Unique identifier in the backend. The unique identifier must never change after the ProductSize has been created. The unique identifier is not visible for the user.
``ProductSizeName``
    A name that represents the product size like 'Large'.
``ProductSizeNo``
    A number or string that represents the product size like 'L'. 
``Sort``
    A number that desides the sorting order within the product sizes.