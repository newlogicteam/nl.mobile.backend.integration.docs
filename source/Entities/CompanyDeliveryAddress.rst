CompanyDeliveryAddress
======================

This entity is used to manage alternative delivery addresses for a company.

Company delivery addresses are used in NewLogic Sales to select an alternative delivery address when creating an order.

Properties
^^^^^^^^^^

``ExternalCompanyDeliveryAddressID``
    Unique identifier in the backend. The unique identifier must never change after the CompanyDeliveryAddress has been created. The unique identifier is not visible for the user.
``ExternalCompanyID``
    The unique identifier that represents the company that should be assigned to the delivery address.
``CompanyDeliveryAddressName``
    The name that is shown to the user in a delivery address picklist. Sometimes the company name is the same on all delivery addresses. In that case, you could use the city to set this property.
``CompanyName``
    ...
``Address1``
    ...
``Address2``
    ...
``City``
    ...
``PostalCode``
    ...
``State``
    ...
``CountryName``
    ...
``PhoneNumber``
    ...
``EmailAddress``
    ...
``Website``
    ...