ProductColor
============

This entity is used to manage product colors.

.. Attention:: Product colors is normally only used when NewLogic Sales is setup to show product models with variants. 
               The variants are combinations of product sizes and product colors. 

A `Product <Product.html>`_ can be linked to a product color by the ``ExternalProductColorID`` property.

Properties
^^^^^^^^^^

``ExternalProductColorID``
    Unique identifier in the backend. The unique identifier must never change after the ProductColor has been created. The unique identifier is not visible for the user.
``ProductColorName``
    A name that represents the product color like 'Red'.
``ProductColorNo``
    A number or string that represents the product color. 
``Sort``
    A number that desides the sorting order within the product colors.