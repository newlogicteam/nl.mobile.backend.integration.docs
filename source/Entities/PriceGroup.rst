PriceGroup
==========

This entity is used to manage price groups.

A price group holds product prices in different currencies. 
A `Company <Company.html>`_ can be linked to a price group by the ``ExternalPriceGroupID`` property.
The ``CurrencyCode`` property is used to reference a product price within the price group for a given currency.

.. Note:: To show product prices in NewLogic Sales, you must create at least one price group like 'Default pricegroup".

Properties
^^^^^^^^^^

``ExternalPriceGroupID``
    Unique identifier in the backend. The unique identifier must never change after the PriceGroup has been created. The unique identifier is not visible for the user.
``PriceGroupName``
    A name that represents the price group.
``PriceGroupNo``
    A number or string that represents the price group.
``Sort``
    Not used. Should be set to 0.
