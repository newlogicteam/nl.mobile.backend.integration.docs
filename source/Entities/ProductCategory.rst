ProductCategory
===============

This entity is used to manage product categories.

A product category can be used in NewLogic Sales to filter and sort products and product models. 
A `Product <Product.html>`_ and `ProductModel <ProductModel.html>`_ can be linked to a product category by the ``ExternalProductCategoryID`` property.

.. Note:: NewLogic Sales can be setup to show product models with variants. When this setup is used, 
          it is not nessesary to link the `Product <Product.html>`_ to a product category. 
          Only the `ProductModel <ProductModel.html>`_ needs to be linked.

Properties
^^^^^^^^^^

``ExternalProductCategoryID``
    Unique identifier in the backend. The unique identifier must never change after the ProductCategory has been created. 
    The unique identifier is not visible for the user.
``ProductCategoryName``
    A name that represents the product category.
``ProductCategoryNo``
    A number or string that represents the product category. 
``Sort``
    A number that desides the sorting order within the product categories.
