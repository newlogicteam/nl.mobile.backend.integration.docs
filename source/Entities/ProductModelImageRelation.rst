ProductModelImageRelation
=========================

This entity is used to manage product model image relations. 

.. Attention:: Product model image relations is normally only used when NewLogic Sales is setup to show product models with variants.

Properties
^^^^^^^^^^
