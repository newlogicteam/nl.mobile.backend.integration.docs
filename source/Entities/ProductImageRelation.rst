ProductImageRelation
====================

This entity is used to manage product image relations. A product can have multiple images and the same image can be used on several products.
