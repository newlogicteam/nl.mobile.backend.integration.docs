Getting started
============

.. toctree::
    :titlesonly:

    Install the API client
    Connect to the API service
    Create single entity
    Create multiple entities


