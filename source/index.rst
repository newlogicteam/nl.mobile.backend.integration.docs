.. NewLogic Mobile IntegrationApi documentation master file, created by
   sphinx-quickstart on Wed Sep 21 16:16:18 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NewLogic Mobile IntegrationApi's documentation!
========================================================================

Contents:

.. toctree::
    :titlesonly:
   
    Introduction
    Getting started/Index
    Entities/Index
    How to/Index
   