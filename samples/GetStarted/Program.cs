﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NL.Mobile.Integration.ApiClient;
using NL.Mobile.Integration.ApiClient.Exceptions;
using NL.Mobile.Integration.ApiModels;
using NL.Mobile.Integration.ApiModels.Requests;

namespace GetStarted
{
	class Program
	{
		private static IntegrationApiClient _apiClient;

		static void Main(string[] args)
		{
			// Set configuration values.
			var apiKey = "";
			var username = "";
			var password = "";
			var accountID = "";

			// Create client.
			_apiClient = new IntegrationApiClient(apiKey, username, password, accountID);

			// Execute integration methods.
			try
			{
				ImportUsers();
				ImportPriceGroups();
				ImportStocks();
				ImportCompanies();
				ImportCompanyUserRelations();
				ImportProducts();
				ImportProductModels();
				ImportProductPrices();
				ImportStockItems();

				ExportOrders();
			}
			finally
			{
				// Make sure to dispose the client when the program ends.
				if (_apiClient != null)
				{
					_apiClient.Dispose();
				}
			}

			Console.WriteLine();
			Console.WriteLine("Press enter to exit...");
			Console.ReadLine();
		}

		static void ImportUsers()
		{
			Console.WriteLine("###### ImportUsers ######");

			// Load users from database, files etc. and map to import requests.
			var importRequests = new List<UserImportRequest>();
			var user1 = new UserImportRequest()
			{
				ExternalUserID = "1",
				UserName = "user1",
				FullName = "User One",
				Email = "user1@test.com",
				CurrencyCode = "EUR",
				PhoneNumber = "12345678",
				ExternalStockID = "Stock1",
				ExternalPriceGroupID = "PriceGroup1",
			};
			importRequests.Add(user1);

			try
			{
				// Create bulk import request and specify if it should delete all unprovided.
				// This is useful if you for example run a full import nightly.
				var importBulkRequest = new UserImportBulkRequest(importRequests, deleteUnprovided: true);

				// Send the import request.
				Console.WriteLine("Sending import request");
				// ImportBulk can throw IntegrationRequestException
				var jobReference = _apiClient.User.ImportBulk(importBulkRequest);

				Console.WriteLine("Awaiting job success");
				// AwaitSuccess can throw IntegrationJobFailedException
				jobReference.AwaitSuccess();
				Console.WriteLine("Job succeeded");
			}
			catch (IntegrationRequestException ex)
			{
				LogIntegrationRequestException(ex);
			}
			catch (IntegrationJobFailedException ex)
			{
				LogIntegrationJobFailedException(ex);
			}

			Console.WriteLine("Finished");
			Console.WriteLine();
		}

		static void ImportPriceGroups()
		{
			Console.WriteLine("###### ImportPriceGroups ######");

			var importRequests = new List<PriceGroupImportRequest>();
			var priceGroup1 = new PriceGroupImportRequest()
			{
				ExternalPriceGroupID = "PriceGroup1",
				PriceGroupName = "Price group 1",
				Sort = 0,
			};
			importRequests.Add(priceGroup1);

			try
			{
				var importBulkRequest = new PriceGroupImportBulkRequest(importRequests, deleteUnprovided: true);
				Console.WriteLine("Sending import request");
				var jobReference = _apiClient.PriceGroup.ImportBulk(importBulkRequest);
				Console.WriteLine("Awaiting job success");
				jobReference.AwaitSuccess();
				Console.WriteLine("Job succeeded");
			}
			catch (IntegrationRequestException ex)
			{
				LogIntegrationRequestException(ex);
			}
			catch (IntegrationJobFailedException ex)
			{
				LogIntegrationJobFailedException(ex);
			}

			Console.WriteLine("Finished");
			Console.WriteLine();
		}

		static void ImportStocks()
		{
			Console.WriteLine("###### ImportStocks ######");

			var importRequests = new List<StockImportRequest>();
			var stock1 = new StockImportRequest()
			{
				ExternalStockID = "Stock1",
				StockName = "Stock 1",
				Sort = 0,
			};

			try
			{
				var importBulkRequest = new StockImportBulkRequest(importRequests, deleteUnprovided: true);
				Console.WriteLine("Sending import request");
				var jobReference = _apiClient.Stock.ImportBulk(importBulkRequest);
				Console.WriteLine("Awaiting job success");
				jobReference.AwaitSuccess();
				Console.WriteLine("Job succeeded");
			}
			catch (IntegrationRequestException ex)
			{
				LogIntegrationRequestException(ex);
			}
			catch (IntegrationJobFailedException ex)
			{
				LogIntegrationJobFailedException(ex);
			}

			Console.WriteLine("Finished");
			Console.WriteLine();
		}

		static void ImportCompanies()
		{
			Console.WriteLine("###### ImportCompanies ######");

			var importRequests = new List<CompanyImportRequest>();
			var company1 = new CompanyImportRequest()
			{
				ExternalCompanyID = "Company1",
				CompanyNo = "1",
				CompanyName = "NewLogic ApS",
				Address1 = "Søren Frichs Vej 38K, 1",
				PostalCode = "8230",
				City = "Åbyhøj",
				CountryName = "Denmark",
				EmailAddress = "info@newlogic.dk",
				PhoneNumber = "+45 72144830",
				CurrencyCode = "EUR",
				ExternalPriceGroupID = "PriceGroup1",
				Website = "http://newlogic.dk",
			};
			importRequests.Add(company1);

			try
			{
				var importBulkRequest = new CompanyImportBulkRequest(importRequests, deleteUnprovided: true);
				Console.WriteLine("Sending import request");
				var jobReference = _apiClient.Company.ImportBulk(importBulkRequest);
				Console.WriteLine("Awaiting job success");
				jobReference.AwaitSuccess();
				Console.WriteLine("Job succeeded");
			}
			catch (IntegrationRequestException ex)
			{
				LogIntegrationRequestException(ex);
			}
			catch (IntegrationJobFailedException ex)
			{
				LogIntegrationJobFailedException(ex);
			}

			Console.WriteLine("Finished");
			Console.WriteLine();
		}

		static void ImportCompanyUserRelations()
		{
			Console.WriteLine("###### ImportCompanyUserRelations ######");

			var importRequests = new List<CompanyUserRelationImportRequest>();
			var request1 = new CompanyUserRelationImportRequest()
			{
				ExternalCompanyID = "Company1",
				ExternalUserID = "User1",
			};
			importRequests.Add(request1);

			try
			{
				var importBulkRequest = new CompanyUserRelationImportBulkRequest(importRequests, deleteUnprovided: true);
				Console.WriteLine("Sending import request");
				var jobReference = _apiClient.CompanyUserRelation.ImportBulk(importBulkRequest);
				Console.WriteLine("Awaiting job success");
				jobReference.AwaitSuccess();
				Console.WriteLine("Job succeeded");
			}
			catch (IntegrationRequestException ex)
			{
				LogIntegrationRequestException(ex);
			}
			catch (IntegrationJobFailedException ex)
			{
				LogIntegrationJobFailedException(ex);
			}

			Console.WriteLine("Finished");
			Console.WriteLine();
		}

		static void ImportProducts()
		{
			Console.WriteLine("###### ImportProducts ######");

			var importRequests = new List<ProductImportRequest>();
			var product1 = new ProductImportRequest()
			{
				ExternalProductID = "Product1",
				ProductNo = "1",
				ProductTitle = "Some product",
				ShortDescription = "Description...",
				LongDescription = "",
				ExternalProductModelID = "Model1"
			};
			importRequests.Add(product1);

			try
			{
				var importBulkRequest = new ProductImportBulkRequest(importRequests, deleteUnprovided: true);
				Console.WriteLine("Sending import request");
				var jobReference = _apiClient.Product.ImportBulk(importBulkRequest);
				Console.WriteLine("Awaiting job success");
				jobReference.AwaitSuccess();
				Console.WriteLine("Job succeeded");
			}
			catch (IntegrationRequestException ex)
			{
				LogIntegrationRequestException(ex);
			}
			catch (IntegrationJobFailedException ex)
			{
				LogIntegrationJobFailedException(ex);
			}

			Console.WriteLine("Finished");
			Console.WriteLine();
		}

		static void ImportProductModels()
		{
			Console.WriteLine("###### ImportProductModels ######");

			var importRequests = new List<ProductModelImportRequest>();
			var product1 = new ProductModelImportRequest()
			{
				ExternalProductModelID = "Model1",
				ProductModelNo = "1",
				ProductModelTitle = "Some Model",
				ShortDescription = "Description...",
				LongDescription = "",
			};
			importRequests.Add(product1);

			try
			{
				var importBulkRequest = new ProductModelImportBulkRequest(importRequests, deleteUnprovided: true);
				Console.WriteLine("Sending import request");
				var jobReference = _apiClient.ProductModel.ImportBulk(importBulkRequest);
				Console.WriteLine("Awaiting job success");
				jobReference.AwaitSuccess();
				Console.WriteLine("Job succeeded");
			}
			catch (IntegrationRequestException ex)
			{
				LogIntegrationRequestException(ex);
			}
			catch (IntegrationJobFailedException ex)
			{
				LogIntegrationJobFailedException(ex);
			}

			Console.WriteLine("Finished");
			Console.WriteLine();
		}

		static void ImportStockItems()
		{
			Console.WriteLine("###### ImportStockItems ######");

			var importRequests = new List<StockItemImportRequest>();
			var stockItem1 = new StockItemImportRequest()
			{
				ExternalStockID = "Stock1",
				ExternalProductID = "Product1",
				Quantity = 2,
				StockStatusSymbolType = StockStatusSymbolType.Green,
				StockStatusValueType = StockStatusValueType.Quantity,
			};

			try
			{
				var importBulkRequest = new StockItemImportBulkRequest(importRequests, deleteUnprovided: true);
				Console.WriteLine("Sending import request");
				var jobReference = _apiClient.StockItem.ImportBulk(importBulkRequest);
				Console.WriteLine("Awaiting job success");
				jobReference.AwaitSuccess();
				Console.WriteLine("Job succeeded");
			}
			catch (IntegrationRequestException ex)
			{
				LogIntegrationRequestException(ex);
			}
			catch (IntegrationJobFailedException ex)
			{
				LogIntegrationJobFailedException(ex);
			}

			Console.WriteLine("Finished");
			Console.WriteLine();
		}

		static void ImportProductPrices()
		{
			Console.WriteLine("###### ImportProductPrices ######");

			var importRequests = new List<ProductPriceImportRequest>();
			var stockItem1 = new ProductPriceImportRequest()
			{
				ExternalProductID = "Product1",
				ExternalPriceGroupID = "PriceGroup1",
				CurrencyCode = "EUR",
				SalesPrice = 199,
			};

			try
			{
				var importBulkRequest = new ProductPriceImportBulkRequest(importRequests, deleteUnprovided: true);
				Console.WriteLine("Sending import request");
				var jobReference = _apiClient.ProductPrice.ImportBulk(importBulkRequest);
				Console.WriteLine("Awaiting job success");
				jobReference.AwaitSuccess();
				Console.WriteLine("Job succeeded");
			}
			catch (IntegrationRequestException ex)
			{
				LogIntegrationRequestException(ex);
			}
			catch (IntegrationJobFailedException ex)
			{
				LogIntegrationJobFailedException(ex);
			}

			Console.WriteLine("Finished");
			Console.WriteLine();
		}

		static void ExportOrders()
		{
			Console.WriteLine("###### ExportOrders ######");
			try
			{
				while (true)
				{
					// Fetch order export from queue if any.
					var orderExportQueueMessage = _apiClient.Order.ExportQueue.Fetch();
					if (orderExportQueueMessage == null)
					{
						// Break loop if there were no pending exports in the queue. 
						break;
					}

					if (orderExportQueueMessage.FetchCount > 1)
					{
						// Export has been fetched before... 
						// Then it would be a good idea to check if the order has already been imported into ERP for example.
					}
					else
					{
						// Import into ERP etc.
						var orderEntity = orderExportQueueMessage.Entity;
					}

					// Complete the export to remove it from the queue.
					_apiClient.Order.ExportQueue.Complete(orderExportQueueMessage.MessageID);
				}
			}
			catch (IntegrationRequestException ex)
			{
				LogIntegrationRequestException(ex);
			}

			Console.WriteLine("Finished");
			Console.WriteLine();
		}

		static void LogIntegrationRequestException(IntegrationRequestException ex)
		{
			Console.WriteLine("---- IntegrationRequestException occured ----");
			Console.WriteLine("Exception.Message: {0}", ex.Message);
			Console.WriteLine("Exception.HttpStatusCode: {0}", ex.HttpStatusCode);
			Console.WriteLine("Exception.HttpStatusMessage: {0}", ex.HttpStatusMessage);
			if (ex.Error != null)
			{
				Console.WriteLine("Exception.Error.Message: {0}", ex.Error.Message);
				Console.WriteLine("Exception.Error.Code: {0}", ex.Error.Code);
				if (ex.Error.Details != null)
				{
					for (int i = 0; i < ex.Error.Details.Count(); i++)
					{
						var detail = ex.Error.Details.ElementAt(i);
						Console.WriteLine("Exception.Job.Error.Details[{0}].Message: {1}", i, detail.Message);
						Console.WriteLine("Exception.Job.Error.Details[{0}].Code: {1}", i, detail.Code);
						Console.WriteLine("Exception.Job.Error.Details[{0}].Reference: {1} ", i, detail.Reference.ToString());
					}
				}
			}
			Console.WriteLine();
		}

		static void LogIntegrationJobFailedException(IntegrationJobFailedException ex)
		{
			Console.WriteLine("---- IntegrationJobFailedException occured ----");
			Console.WriteLine("Exception.Message: {0}", ex.Message);
			Console.WriteLine("Exception.Job.Error.Message: {0}", ex.Job.Error.Message);
			Console.WriteLine("Exception.Job.Error.Code: {0}", ex.Job.Error.Code);
			if (ex.Job.Error.Details != null)
			{
				for (int i = 0; i < ex.Job.Error.Details.Count(); i++)
				{
					var detail = ex.Job.Error.Details.ElementAt(i);
					Console.WriteLine("Exception.Job.Error.Details[{0}].Message: {1}", i, detail.Message);
					Console.WriteLine("Exception.Job.Error.Details[{0}].Code: {1}", i, detail.Code);
					Console.WriteLine("Exception.Job.Error.Details[{0}].Reference: {1} ", i, detail.Reference.ToString());
				}
			}
			Console.WriteLine();
		}
	}
}
